# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/lazy_heat_map'

RSpec.describe LazyHeatMap do
  let(:instance) { described_class.new(resources, policy_spec, mock_network) }

  let(:options) { Struct.new(:source, :source_id, :host_url, :api_version) }

  let(:host_url) { 'https://gitlab.com' }
  let(:resource_path) { 'g/p' }
  let(:network_options) do
    options.new(:projects, resource_path)
  end

  let(:mock_network) { double(options: network_options) }
  let(:resource_base) do
    {
      type: 'issues',
      web_url: 'https://gitlab.com/g/p/-/issues/1'
    }
  end

  let(:query_base_url) { 'https://gitlab.com/g/p/-/issues?state=open&label_name%5B%5D=bug&label_name%5B%5D=Category&label_name%5B%5D=' }

  let(:resources) do
    [
      resource_base.merge(labels: %w[priority::1 severity::2], assignees: [], milestone: nil),
      resource_base.merge(labels: %w[priority::3 severity::3], assignees: [], milestone: nil),
      resource_base.merge(labels: %w[priority::2], assignees: [], milestone: nil),
      resource_base.merge(labels: %w[severity::3], assignees: [], milestone: nil),
      resource_base.merge(labels: %w[], assignees: [], milestone: nil)
    ]
  end

  let(:policy_spec) do
    { conditions: { state: 'open', labels: %w[bug Category] } }
  end

  before do
    allow(mock_network).to receive(:query_api_cached).and_return(
      [
        { web_url: "#{host_url}/#{resource_path}" }
      ]
    )
  end

  describe '#generate_heat_map_table' do
    subject { instance.generate_heat_map_table }

    it 'generates the heatmap' do
      p1s2 = "[1](#{query_base_url}priority%3A%3A1&label_name%5B%5D=severity%3A%3A2)"
      p3s3 = "[1](#{query_base_url}priority%3A%3A3&label_name%5B%5D=severity%3A%3A3)"
      p2 = "[1](#{query_base_url}priority%3A%3A2&not%5Blabel_name%5D%5B%5D=severity%3A%3A1&not%5Blabel_name%5D%5B%5D=severity%3A%3A2&not%5Blabel_name%5D%5B%5D=severity%3A%3A3&not%5Blabel_name%5D%5B%5D=severity%3A%3A4)"
      s3 = "[1](#{query_base_url}severity%3A%3A3&not%5Blabel_name%5D%5B%5D=priority%3A%3A1&not%5Blabel_name%5D%5B%5D=priority%3A%3A2&not%5Blabel_name%5D%5B%5D=priority%3A%3A3&not%5Blabel_name%5D%5B%5D=priority%3A%3A4)"
      neither = "[1](https://gitlab.com/g/p/-/issues?state=open&label_name%5B%5D=bug&label_name%5B%5D=Category&not%5Blabel_name%5D%5B%5D=priority%3A%3A1&not%5Blabel_name%5D%5B%5D=priority%3A%3A2&not%5Blabel_name%5D%5B%5D=priority%3A%3A3&not%5Blabel_name%5D%5B%5D=priority%3A%3A4&not%5Blabel_name%5D%5B%5D=severity%3A%3A1&not%5Blabel_name%5D%5B%5D=severity%3A%3A2&not%5Blabel_name%5D%5B%5D=severity%3A%3A3&not%5Blabel_name%5D%5B%5D=severity%3A%3A4)"

      expect(subject).to eq(<<~MARKDOWN.chomp)
        || ~"severity::1" | ~"severity::2" | ~"severity::3" | ~"severity::4" | No severity |
        |----|----|----|----|----|----|
        | ~"priority::1" | 0 | #{p1s2} | 0 | 0 | 0 |
        | ~"priority::2" | 0 | 0 | 0 | 0 | #{p2} |
        | ~"priority::3" | 0 | 0 | #{p3s3} | 0 | 0 |
        | ~"priority::4" | 0 | 0 | 0 | 0 | 0 |
        | No priority | 0 | 0 | #{s3} | 0 | #{neither} |
      MARKDOWN
    end

    context 'when all of resources have priorities and severities' do
      let(:resources) do
        [
          resource_base.merge(labels: %w[priority::1 severity::4]),
          resource_base.merge(labels: %w[priority::1 severity::4]),
          resource_base.merge(labels: %w[priority::1 severity::4]),
          resource_base.merge(labels: %w[priority::1 severity::4]),
          resource_base.merge(labels: %w[priority::1 severity::4])
        ]
      end

      it 'still shows No priorities/severities columns' do
        p1s4 = "[5](#{query_base_url}priority%3A%3A1&label_name%5B%5D=severity%3A%3A4)"

        expect(subject).to eq(<<~MARKDOWN.chomp)
          || ~"severity::1" | ~"severity::2" | ~"severity::3" | ~"severity::4" | No severity |
          |----|----|----|----|----|----|
          | ~"priority::1" | 0 | 0 | 0 | #{p1s4} | 0 |
          | ~"priority::2" | 0 | 0 | 0 | 0 | 0 |
          | ~"priority::3" | 0 | 0 | 0 | 0 | 0 |
          | ~"priority::4" | 0 | 0 | 0 | 0 | 0 |
          | No priority | 0 | 0 | 0 | 0 | 0 |
        MARKDOWN
      end
    end

    context 'when for a project' do
      it 'url contains project path' do
        expect(subject).to include("gitlab.com/g/p/-/issues")
      end

      context 'when for a numeric project ID' do
        let(:project_id) { 123 }
        let(:network_options) do
          options.new(:projects, project_id)
        end

        it 'url contains project path' do
          expect(subject).to include("gitlab.com/g/p/-/issues")
        end
      end

      context 'when for a subgroup project' do
        let(:resource_path) { 'group/subgroup/project' }

        it 'url contains project path' do
          expect(subject).to include("gitlab.com/group/subgroup/project/-/issues")
        end
      end
    end

    context 'when for a group' do
      before do
        allow(mock_network).to receive(:query_api_cached).and_return(
          [
            { web_url: "#{host_url}/groups/#{resource_path}" }
          ]
        )
      end

      let(:resource_path) { 'g' }
      let(:network_options) do
        options.new(:groups, resource_path)
      end

      it 'url contains group path' do
        expect(subject).to include("gitlab.com/groups/g/-/issues")
      end

      context 'when for a numeric group ID' do
        let(:group_id) { 123 }
        let(:network_options) do
          options.new(:groups, group_id)
        end

        it 'url contains group path' do
          expect(subject).to include("gitlab.com/groups/g/-/issues")
        end
      end

      context 'when for a subgroup group' do
        let(:resource_path) { 'group/subgroup' }

        it 'url contains project path' do
          expect(subject).to include("gitlab.com/groups/group/subgroup/-/issues")
        end
      end
    end
  end

  describe '#generate_heat_map_table_stuck' do
    subject { instance.generate_heat_map_table_stuck }

    let(:resources) do
      [
        resource_base.merge(labels: %w[priority::1 severity::2], assignees: [], milestone: '16.2'),
        resource_base.merge(labels: %w[priority::3 severity::3], assignees: [], milestone: nil),
        resource_base.merge(labels: %w[priority::2], assignees: [], milestone: nil),
        resource_base.merge(labels: %w[severity::3], assignees: [], milestone: nil),
        resource_base.merge(labels: %w[], assignees: [], milestone: nil)
      ]
    end

    context 'when an assignee is not set' do
      let(:resources) do
        [resource_base.merge(labels: %w[priority::1 severity::2], assignees: [], milestone: { title: '14.4' })]
      end

      let(:expected_url) do
        "#{query_base_url}severity%3A%3A2&assignee_id=None"
      end

      it 'renders the table with the correct link' do
        expect(subject).to eq(<<~MARKDOWN.chomp)
          || ~"severity::1" | ~"severity::2" | ~"severity::3" | ~"severity::4" | No severity |
          |----|----|----|----|----|----|
          | No assignees | 0 | [1](#{expected_url}) | 0 | 0 | 0 |
          | No milestone | 0 | 0 | 0 | 0 | 0 |
          | Milestone: Backlog | 0 | 0 | 0 | 0 | 0 |
          | Milestone in the past | 0 | 0 | 0 | 0 | 0 |
        MARKDOWN
      end
    end

    context 'when no milestones are set' do
      let(:resources) do
        [resource_base.merge(labels: %w[priority::1 severity::2], assignees: ['@user1'], milestone: nil)]
      end

      let(:expected_url) do
        "#{query_base_url}severity%3A%3A2&milestone_title=None"
      end

      it 'renders the table with the correct link' do
        expect(subject).to eq(<<~MARKDOWN.chomp)
          || ~"severity::1" | ~"severity::2" | ~"severity::3" | ~"severity::4" | No severity |
          |----|----|----|----|----|----|
          | No assignees | 0 | 0 | 0 | 0 | 0 |
          | No milestone | 0 | [1](#{expected_url}) | 0 | 0 | 0 |
          | Milestone: Backlog | 0 | 0 | 0 | 0 | 0 |
          | Milestone in the past | 0 | 0 | 0 | 0 | 0 |
        MARKDOWN
      end
    end

    context 'when the milestone is Backlog' do
      let(:resources) do
        [resource_base.merge(labels: %w[priority::1 severity::2], assignees: ['@user1'], milestone: { title: 'Backlog' })]
      end

      let(:expected_url) do
        "#{query_base_url}severity%3A%3A2&milestone_title=Backlog"
      end

      it 'renders the table with the correct link' do
        expect(subject).to eq(<<~MARKDOWN.chomp)
          || ~"severity::1" | ~"severity::2" | ~"severity::3" | ~"severity::4" | No severity |
          |----|----|----|----|----|----|
          | No assignees | 0 | 0 | 0 | 0 | 0 |
          | No milestone | 0 | 0 | 0 | 0 | 0 |
          | Milestone: Backlog | 0 | [1](#{expected_url}) | 0 | 0 | 0 |
          | Milestone in the past | 0 | 0 | 0 | 0 | 0 |
        MARKDOWN
      end
    end

    context 'when the milestone is in the past' do
      let(:resources) do
        [resource_base.merge(labels: %w[priority::1 severity::2], assignees: ['@user1'], milestone: { expired: true })]
      end

      let(:expected_url) do
        "#{query_base_url}severity%3A%3A2&not%5Bmilestone_title%5D=Upcoming"
      end

      it 'renders the table with the correct link' do
        expect(subject).to eq(<<~MARKDOWN.chomp)
          || ~"severity::1" | ~"severity::2" | ~"severity::3" | ~"severity::4" | No severity |
          |----|----|----|----|----|----|
          | No assignees | 0 | 0 | 0 | 0 | 0 |
          | No milestone | 0 | 0 | 0 | 0 | 0 |
          | Milestone: Backlog | 0 | 0 | 0 | 0 | 0 |
          | Milestone in the past | 0 | [1](#{expected_url}) | 0 | 0 | 0 |
        MARKDOWN
      end
    end

    context 'when no severity is set and in Backlog' do
      let(:resources) do
        [resource_base.merge(labels: %w[priority::1], assignees: ['@user1'], milestone: { title: 'Backlog' })]
      end

      let(:expected_url) do
        "#{query_base_url.delete_suffix('label_name%5B%5D=')}not%5Blabel_name%5D%5B%5D=severity%3A%3A1&not%5Blabel_name%5D%5B%5D=severity%3A%3A2&not%5Blabel_name%5D%5B%5D=severity%3A%3A3&not%5Blabel_name%5D%5B%5D=severity%3A%3A4&milestone_title=Backlog"
      end

      it 'renders the table with the correct link' do
        expect(subject).to eq(<<~MARKDOWN.chomp)
          || ~"severity::1" | ~"severity::2" | ~"severity::3" | ~"severity::4" | No severity |
          |----|----|----|----|----|----|
          | No assignees | 0 | 0 | 0 | 0 | 0 |
          | No milestone | 0 | 0 | 0 | 0 | 0 |
          | Milestone: Backlog | 0 | 0 | 0 | 0 | [1](#{expected_url}) |
          | Milestone in the past | 0 | 0 | 0 | 0 | 0 |
        MARKDOWN
      end
    end
  end
end

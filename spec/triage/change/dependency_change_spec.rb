# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/triage/change/dependency_change'

RSpec.describe Triage::DependencyChange do
  describe '.file_patterns' do
    it 'matches dependency files' do
      files = %w[
        Gemfile.lock
        vendor/gems/attr_encrypted/Gemfile.lock
        workhorse/go.sum
        yarn.lock
        storybook/yarn.lock
      ]

      expect(files).to all(match(described_class.file_patterns))
    end
  end

  describe '.line_patterns' do
    it 'matches anything' do
      diffs = [
        '+ gem',
        '+ content-type',
        '- gem',
        '- content-type'
      ]

      expect(diffs).to all(match(described_class.line_patterns))
    end
  end
end

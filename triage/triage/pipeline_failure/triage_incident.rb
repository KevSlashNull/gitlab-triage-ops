# frozen_string_literal: true

require_relative '../../../triage/triage'
require_relative '../../../triage/resources/ci_job'
require_relative '../../../lib/constants/labels'
require_relative '../../../lib/www_gitlab_com'
require_relative 'failure_trace'
require_relative 'pipeline_incident_finder'

module Triage
  module PipelineFailure
    class TriageIncident
      ROOT_CAUSE_LABELS = Labels::MASTER_BROKEN_ROOT_CAUSE_LABELS
      LABELS_FOR_TRANSIENT_ERRORS = Labels::MASTER_BROKEN_ROOT_CAUSE_LABELS.values_at(
        :failed_to_pull_image,
        :gitlab_com_overloaded,
        :runner_disk_full,
        :infrastructure,
        :job_timeout
      ).freeze

      POST_RETRY_JOB_URL_THRESHOLD = 10
      FAILURE_TRACE_SIZE_LIMIT = 50000 # Keep it processable by human
      RSPEC_DURATION_COMMENT_LIMIT = 800000

      FRONTEND_CHANNEL = 'frontend'

      attr_reader :event, :config, :failed_jobs

      def initialize(event:, config:, failed_jobs:)
        @event = event
        @config = config
        @failed_jobs = failed_jobs
      end

      def top_root_cause_label
        return ROOT_CAUSE_LABELS[:default] if triaged_jobs.empty?

        # find the top root cause label preferably not master-broken::undetermined
        potential_root_cause_labels
          .tally
          .max_by { |label, count| label == ROOT_CAUSE_LABELS[:default] ? 0 : count }[0]
      end

      def top_group_label
        return if triaged_jobs.empty? || potential_group_labels.empty?

        potential_group_labels
          .tally
          .max_by { |_label, count| count }[0]
      end

      def root_cause_analysis_comment
        return if triaged_jobs.empty?

        [
          list_all_triaged_jobs_comment,
          retry_pipeline_comment,
          incident_modifier_comment
        ].compact.join("\n\n").prepend("\n\n")
      end

      def investigation_comment
        return if triaged_jobs.empty? || closeable?

        jobs_with_failed_tests = triaged_jobs.reject { |triaged_job| triaged_job[:failed_test_trace].nil? }
        return if jobs_with_failed_tests.empty?

        jobs_with_failed_tests.map do |triaged_job|
          <<~MARKDOWN.chomp
            - #{triaged_job[:link]}:

            #{triaged_job[:failed_test_trace]}
          MARKDOWN
        end.join("\n\n").prepend("\n\n")
      end

      def duration_analysis_comment
        return if triaged_jobs.empty?

        jobs_with_rspec_run_time_summary = triaged_jobs.reject do |triaged_job|
          triaged_job[:rspec_run_time_summary].nil?
        end

        return if jobs_with_rspec_run_time_summary.empty?

        duration_data_array = jobs_with_rspec_run_time_summary.map do |triaged_job|
          <<~MARKDOWN.chomp
            - #{triaged_job[:link]}:

            #{triaged_job[:rspec_run_time_summary]}
          MARKDOWN
        end

        string_dump = ''
        index_exceeding_limit = duration_data_array.reduce(0) do |index, data|
          string_dump += data
          break index if string_dump.size > rspec_duration_comment_limit

          index + 1
        end

        duration_data_array[0...index_exceeding_limit]
          .join("\n\n")
          .prepend("\n\n")
          .delete_suffix("\n\n") # removes any trailing '\n\n'
      end

      def attribution_comment
        return if triaged_jobs.empty?

        attributed_jobs = triaged_jobs.map do |triaged_job|
          <<~MARKDOWN.chomp
            #{triaged_job[:attribution_message]}
          MARKDOWN
        end.uniq.reject(&:empty?)

        message_body = attributed_jobs.empty? ? '' : attributed_jobs.join("\n\n").prepend("\n\n")
        attributed_slack_channel_info.present? ? message_body + attributed_slack_channel_info : message_body
      end

      def duplicate_incident_url
        return @duplicate_incident_url if defined?(@duplicate_incident_url)

        @duplicate_incident_url = nil
        failed_job_names = failed_jobs.map(&:name)

        duplicate_incident = previous_incidents.find do |previous_incident|
          # if the previous incident was closed as a duplicate of another incident
          # and the current incident's failed jobs are a subset of the previous incident's ones
          # we mark the current incident as a duplicate as well
          previous_failed_job_names = previous_incident.title.split('with ').last.split(', ')

          Set.new(failed_job_names).subset?(Set.new(previous_failed_job_names))
        end

        return unless duplicate_incident

        @duplicate_incident_url = closed_as_duplicate_source_url(duplicate_incident)
      end

      def previous_incidents
        @previous_incidents ||= PipelineIncidentFinder.new(incident_project_id: config.canonical_incident_project_id).incidents
      end

      def duplicate?
        !!duplicate_incident_url
      end

      def closeable?
        duplicate? || all_jobs_failed_with_transient_errors?
      end

      private

      def ci_jobs
        @ci_jobs = failed_jobs.to_h do |job|
          [job.id, Triage::CiJob.new(instance: event.instance, project_id: event.project_id, job_id: job.id, job_name: job.name)]
        end
      end

      def list_all_triaged_jobs_comment
        return if triaged_jobs.empty?

        triaged_jobs.map do |triaged_job|
          %(- #{triaged_job[:link]}: ~"#{triaged_job[:label]}".#{retry_job_comment(triaged_job)})
        end.join("\n")
      end

      def triaged_jobs
        @triaged_jobs ||=
          failed_jobs.map do |job|
            failure_trace = FailureTrace.new(
              job_trace: ci_jobs.fetch(job.id).trace,
              job_name: job.name,
              failed_examples_data: ci_jobs.fetch(job.id).failed_rspec_test_metadata,
              spec_duration_reports: ci_jobs.fetch(job.id).spec_duration_reports,
              config: config
            )

            {
              id: job.id,
              link: "[#{job.name}](#{job.web_url})",
              label: failure_trace.root_cause_label,
              failed_test_trace: failure_trace.test_failure_summary_markdown,
              attribution_labels: failure_trace.potential_group_labels,
              attribution_message: failure_trace.attribution_message_markdown,
              rspec_run_time_summary: failure_trace.rspec_run_time_summary_markdown
            }
          end
      end

      def retry_job_comment(failed_job)
        retry_job_web_url = retry_job_and_return_web_url_if_applicable(failed_job)
        return if retry_job_web_url.nil?

        " Retried at: #{retry_job_web_url}"
      end

      def retry_job_and_return_web_url_if_applicable(triaged_job)
        return unless transient_error?(triaged_job[:label]) && retry_jobs_individually?

        ci_jobs.fetch(triaged_job[:id]).retry['web_url']
      end

      def transient_error?(root_cause_label)
        LABELS_FOR_TRANSIENT_ERRORS.include?(root_cause_label)
      end

      def retry_jobs_individually?
        # the benefit of retrying individual job is so we can post the retried job url
        # but if there are too many failed jobs
        # it's more practical to click `retry pipeline` and verify the overall pipeline status when all jobs finish
        failed_jobs.size < POST_RETRY_JOB_URL_THRESHOLD
      end

      def retry_pipeline_comment
        return unless all_jobs_failed_with_transient_errors?
        return if retry_jobs_individually?

        retry_pipeline_response = Triage.api_client.retry_pipeline(event.project_id, event.id).to_h

        "Retried pipeline: #{retry_pipeline_response['web_url']}"
      end

      def incident_modifier_comment
        return unless closeable?

        if duplicate?
          format(config.duplicate_command_body, { duplicate_incident_url: duplicate_incident_url })
        else
          format(config.close_command_body)
        end
      end

      def all_jobs_failed_with_transient_errors?
        return @all_jobs_failed_with_transient_errors if defined?(@all_jobs_failed_with_transient_errors)

        @all_jobs_failed_with_transient_errors = (potential_root_cause_labels.uniq - config.transient_root_cause_labels).empty?
      end

      def potential_group_labels
        @potential_group_labels ||= (triaged_jobs.map { |job| job[:attribution_labels] }).flatten
      end

      def closed_as_duplicate_source_url(incident)
        duplicate_link = incident._links['closed_as_duplicate_of']
        return incident.web_url unless duplicate_link

        duplicate_link
      end

      def potential_root_cause_labels
        @potential_root_cause_labels ||= triaged_jobs.map { |job| job[:label] }
      end

      def groups
        @groups ||= WwwGitLabCom.groups
      end

      def top_group
        return unless top_group_label

        _group_key, group_data = groups.find { |_k, group| group['label'] == top_group_label }

        # if the job is not attributed to any group and has frontend error, we attribute it to 'frontend'
        group_data.nil? && top_group_label == Labels::FRONTEND_LABEL ? frontend_group_data : group_data
      end

      def top_group_channel
        return unless top_group

        top_group['slack_channel']
      end

      def attributed_slack_channel_info
        return unless top_group_channel

        <<~MARKDOWN.chomp.prepend("\n\n")
        **This incident is attributed to ~"#{top_group_label}" and posted in `##{top_group_channel}`.**
        MARKDOWN
      end

      def rspec_duration_comment_limit
        RSPEC_DURATION_COMMENT_LIMIT
      end

      def frontend_group_data
        { 'label' => 'frontend', 'slack_channel' => FRONTEND_CHANNEL }
      end
    end
  end
end

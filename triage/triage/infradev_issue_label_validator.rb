# frozen_string_literal: true
require_relative '../triage'
require_relative '../triage/devops_labels_validator'
require_relative '../triage/event'
require_relative '../../lib/constants/labels'
require_relative '../resources/issue'

module Triage
  class InfradevIssueLabelValidator
    INFRADEV_LABEL_NUDGER_CLASS = 'Triage::InfradevIssueLabelNudger'
    attr_reader :event

    def initialize(event)
      @event = event
    end

    def applicable_event?
      (event.new_entity? || event_label_changed?) &&
        event.label_names.include?(Labels::INFRADEV_LABEL) &&
        missing_ownership_label?(event.label_names)
    end

    def applicable_issue?
      issue.open? &&
        issue.labels.include?(Labels::INFRADEV_LABEL) &&
        missing_ownership_label?(issue.labels)
    end

    private

    def event_label_changed?
      event.removed_label_names.any? || event.added_label_names.any?
    end

    def missing_ownership_label?(label_names)
      !DevopsLabelsValidator.new(label_names).labels_set?
    end

    def issue
      @issue ||= Triage::Issue.new(Triage.api_client.issue(event.project_id, event.iid).to_h)
    end
  end
end

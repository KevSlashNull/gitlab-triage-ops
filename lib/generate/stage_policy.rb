# frozen_string_literal: true

require 'erb'
require 'fileutils'

require_relative '../hierarchy/stage'

module Generate
  module StagePolicy
    def self.run(options)
      template_name = File.basename(options.template)
      erb = ERB.new(File.read(options.template), trim_mode: '-')

      FileUtils.rm_rf("#{destination}/#{template_name}", secure: true)
      FileUtils.mkdir_p("#{destination}/#{template_name}")

      Hierarchy::Stage.all.each do |stage|
        next if options.only && !options.only.include?(stage.key)

        raise "Stage #{stage.key} not found!" if stage.nil?

        generated_template_path = "#{destination}/#{template_name}/#{stage.key}.yml"
        puts "Generating #{generated_template_path}"

        File.write(
          generated_template_path,
          erb.result_with_hash(
            stage: stage,
            assignees: stage.assignees(options.assign)
          )
        )
      end
    end

    def self.destination
      @destination ||=
        File.expand_path('generated', "#{__dir__}/../../policies")
    end
  end
end
